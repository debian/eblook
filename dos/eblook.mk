# Valid System Name :  sys=os2, win, dos

CC = wcl386
EBINCDIR=..\..\..\eb\dos
EBLIBDIR=..\..\..\eb\dos\lib

!ifeq sys os2
CF_SYSDEP = -5s -bt=os2 -mf
LF_SYSDEP = SYS os2v2

!else ifeq sys win
CF_SYSDEP = -5s -bt=nt  -mf
LF_SYSDEP = SYS nt   

!else ifeq sys dos
CF_SYSDEP = -5s -bt=dos -mf
CFDLL_SYSDEP = $(CF_SYSDEP)

!else
!error Target System is invalid. "$(sys)"
!endif

CF_COMMON = -i=$(%WATCOM)\h;..;$(EBINCDIR) -w4 -e25 -od -fi..\config.h
LF_COMMON = d all op m op maxe=25


CFLAGS=$(CF_COMMON) $(CF_SYSDEP)
LDFLAGS=$(LF_COMMON) $(LF_SYSDEP)
WCL = $(CFLAGS)
WCL386 = $(WCL)
LD = wlink

LIB = $(EBLIBDIR)\eb$(SYS).lib 

all : ..\$(SYS)\eblook.exe .symbolic
	@echo Finish !!

.EXTENSIONS : .txt
.c: ..;..\..;.
.c.obj: .AUTODEPEND
	$(CC) $(CFLAGS) -c $[@

OBJS = eblook.obj bmp2ppm.obj bmp_in.obj win32.obj getopt.obj
OBJS_C = eblook.obj,bmp2ppm.obj,bmp_in.obj,win32.obj,getopt.obj

..\$(SYS)\eblook.exe : $(OBJS) $(LIB)
	$(LD) $(LDFLAGS) Name $@ File $(OBJS_C) Library $(LIB)

cleanbin : .SYMBOLIC
	-del *.obj
	-del *.exe
	-del *.dll
	-del *.lib

clean : .SYMBOLIC
	-del *.obj
	-del *.txt

pkg : cleanbin all .SYMBOLIC
	-mkdir ..\$(sys)
	-mkdir ..\lib
	-del ..\$(sys)\*.txt
	-del ..\$(sys)\*.exe

	wstrip eblook.exe

	copy *.exe ..\$(sys)
	copy ..\COPYING ..\$(sys)


