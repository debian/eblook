@echo off
rem This batch file is made for command shell on Win NT series.
rem Usage :
rem 	build.bat [cmd] [lib] [arc]
rem	build.bat all
rem	build.bat clean

rem if defined sys goto exit2
set sys=dos win os2

for %%i in (%sys%) do mkdir tmp-%%i
for %%i in (%sys%) do mkdir %%i

if '%1' == 'clean' goto clean

if '%1' == 'all' goto cmd1
if not '%1' == 'cmd' goto arc
shift
:cmd1

for %%i in (%sys%) do (
	cd tmp-%%i
	wmake -f ..\eblook.mk sys=%%i
	cd ..
)

:arc
if '%1' == 'all' goto arc2
if not '%1' == 'arc' goto exit
shift
:arc2

del *.lzh
for %%i in (%sys%) do (
	copy readme.txt %%i
	copy ..\copying %%i
	lha a /r2x1 eblk-%%i.lzh %%i
)

goto exit


:clean
for %%i in (%sys%) do (
	cd tmp-%%i
	wmake -f ..\eblook.mk sys=%%i clean
	cd ..
)

:exit
ECHO Done.
set sys=

:exit2
