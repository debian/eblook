DOS,OS/2(32bit),Windows(32bit) binary 配布版 eblook

この版には大量の unofficial patch が含まれています。
(私の作成したもの、及び澤田石さんの作成されたものなど)
もしバグを発見された方は edict ML または 
tnemoto@mvi.biglobe.ne.jp まで御連絡下さい。

使用法
	Windows 版を除き eblook.exe を path の通ったディレクトリに
	配置するだけです。
	Windows 版は 別途配布の eb library の DLL (libeb.dll) を
	同じディレクトリに配置して下さい。

拡張部概略
	1. 理化学辞典等の inline image を通常の image に変換して表示
	2. 一部文字装飾対応 ( "set decorate-mode on" )
	2. 前後項目への移動対応 ( "set browsing-mode on" )
	3. eblook 単体で使うための拡張 ( "set interactive-mode on" )
		  # ナンバーへのジャンプは "content #1" などとして下さい。
	4. DOS 用パッチなど...         

ToDo:
	GNU readline との link
